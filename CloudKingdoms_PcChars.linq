<Query Kind="Program" />

void Main()
{
    var bytes =
        File.ReadAllBytes(@"R:\retrodev\Drives\C\CLOUD\PCCHARS.EGA")
        .ToArray();
    bytes.Length.ToString("X").Dump();
    const byte bloxPerRow = 40;
    byte[,] pixels = new byte[8 * bloxPerRow, 32];
    
    for (int plane = 0; plane < 4; plane++)
    {
        for (int row = 0; row < 32; row++)
        {
            for (int pixelByte = 0; pixelByte < 40; pixelByte++)
            {
                byte mask = 0b10000000;
                byte data = bytes[plane * 0x500  + row * 40 + pixelByte];
                for (int pixel = 0; pixel < 8; pixel++)
                {
                    pixels[pixelByte * 8 + pixel, row]
                        += ((data & mask) > 0 ? (byte)Math.Pow(2, 3 - plane) : (byte)0);
                    mask >>= 1;
                }
            }
        }
    }
    
    dumpTiles(pixels);
}

private void dumpTiles(byte[,] pixels)
{
    for (int r = 0; r < 32; r++)
    {
		if (r == 16 || r == 16 + 12)
		{
			Console.WriteLine();
			Console.WriteLine();
		}
        var maxX = 8*40;
        for (int x = 0; x < maxX; x++)
        {
            byte pixel = pixels[x, r];
            var red = _palette[pixel, 0];
            var green = _palette[pixel, 1];
            var blue = _palette[pixel, 2];
            Console.Write(Util.WithStyle("██", $"color: RGB({red},{green},{blue})"));
        }
        Console.WriteLine();
    }
}

public void RenderRunOfPixels(string run, byte color)
{
    var red = _palette[color, 0];
    var green = _palette[color, 1];
    var blue = _palette[color, 2];
    Console.Write(Util.WithStyle(run, $"color: RGB({red},{green},{blue})"));
}

byte[,] _palette = new byte[16, 3]
{
    /* 00 */ {0,0,0},
    /* 01 */ {0,0,170},
    /* 02 */ {0,170,0},
    /* 03 */ {0,170,170},
    /* 04 */ {170,0,0},
    /* 05 */ {170,0,170},
    /* 06 */ {170,85,0},
    /* 07 */ {170,170,170},
    /* 08 */ {85,85,85},
    /* 09 */ {85,85,255},
    /* 10 */ {85,255,85},
    /* 11 */ {85,255,255},
    /* 12 */ {255,85,85},
    /* 13 */ {255,85,255},
    /* 14 */ {255,255,85},
    /* 15 */ {255,255,255}
};
