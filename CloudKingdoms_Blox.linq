<Query Kind="Program" />

public class Blox
{
    const byte BLOX_WIDTH = 32;
    const byte BLOX_HEIGHT = 32;
    const byte PIXELS_PER_BYTE = 8;
    const byte BLOX_WIDTH_IN_BYTES = BLOX_WIDTH / PIXELS_PER_BYTE;
    const byte PLANES = 4;

    private byte[] _bytesFromFile;
    
    public Blox()
    {
        LoadFile();
    }

    public void AddBloxToPixels(int bloxIndex, int topLeftX, int topLeftY, byte[,] pixels)
    {
        var offsetInFile = bloxIndex * (BLOX_WIDTH_IN_BYTES * BLOX_HEIGHT * PLANES);

        for (int plane = 0; plane < PLANES; plane++)
        {
            for (int row = 0; row < BLOX_HEIGHT; row++)
            {
                for (int pixelByte = 0; pixelByte < BLOX_WIDTH_IN_BYTES; pixelByte++)
                {
                    byte mask = 0b10000000;
                    byte data = _bytesFromFile[offsetInFile + plane * BLOX_WIDTH_IN_BYTES * BLOX_HEIGHT + row * BLOX_WIDTH_IN_BYTES + pixelByte];
                    for (int pixel = 0; pixel < 8; pixel++)
                    {
                        var pixelX = topLeftX + pixelByte * PIXELS_PER_BYTE + pixel;
                        var pixelY = topLeftY + row;
                        var value = ((data & mask) > 0 ? (byte)Math.Pow(2, 3 - plane) : (byte)0);
                        pixels[pixelX, pixelY] += value;
                        mask >>= 1;
                    }
                }
            }
        }
    }

    private void LoadFile()
    {
        _bytesFromFile =
            File.ReadAllBytes(@"R:\retrodev\Drives\C\CLOUD\PCBLOX1.EGA")
            .Concat(File.ReadAllBytes(@"R:\retrodev\Drives\C\CLOUD\PCBLOX2.EGA"))
            .ToArray();
    }
}