<Query Kind="Program" />

#load ".\CloudKingdoms_MapData"
#load ".\CloudKingdoms_Blox"
#load ".\CloudKingdoms_PixelDisplay"

public void Main()
{
    var mapData = new MapData();
    var blox = new Blox();
    var pixelDisplay = new PixelDisplay(1024, 512);
    var map = mapData.Maps[1];
    
    for (int y = 0; y < 16; y++)
    {
        for (var x = 0; x < 32; x++)
        {
            var bloxIndex = mapData.GetBloxIndexFromMapValue(map[x, y]);
            if (bloxIndex >= 100 && bloxIndex <= 106)
            {
                bloxIndex -= 20;
            }
            else if (bloxIndex == 107)
            {
                bloxIndex = 5;
            }
            blox.AddBloxToPixels(bloxIndex, x * 32, y * 32, pixelDisplay.Pixels);
        }
    }
    pixelDisplay.RenderBitmap();
}