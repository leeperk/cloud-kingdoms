<Query Kind="Program" />

#load ".\CloudKingdoms_PixelDisplay"
#load ".\CloudKingdoms_Blox"

const byte TOTAL_BLOX = 120;
const byte BLOX_PER_ROW = 10;
const byte BORDER_SIZE = 2;
const byte BLOX_WIDTH = 32;
const byte BLOX_HEIGHT = 32;
const byte PIXELS_PER_BYTE = 8;
const byte BLOX_WIDTH_IN_BYTES = BLOX_WIDTH / PIXELS_PER_BYTE;
const byte PLANES = 4;
public const int PIXEL_TOTAL_WIDTH = (BLOX_WIDTH + BORDER_SIZE) * BLOX_PER_ROW;
private static readonly int TOTAL_BLOX_ROWS = (int)Math.Ceiling((double)TOTAL_BLOX / (double)BLOX_PER_ROW);
private static readonly int PIXEL_TOTAL_HEIGHT = (BLOX_HEIGHT + BORDER_SIZE) * TOTAL_BLOX_ROWS;

private PixelDisplay _pixelDisplay = new PixelDisplay(PIXEL_TOTAL_WIDTH, PIXEL_TOTAL_HEIGHT);
private Blox _blox = new Blox();
private byte[] _bytesFromFile;

public void Main()
{
    _bytesFromFile = LoadFile();
    for (int y = 0; y < TOTAL_BLOX_ROWS; y++)
    {
        for (var x = 0; x < BLOX_PER_ROW; x++)
        {
            AddBloxToPixels(x, y);
        }
    }
    //ShowColorsUsage();
    _pixelDisplay.RenderBitmap();
}

private byte[] LoadFile()
{
    var bytesFromFile =
        File.ReadAllBytes(@"R:\retrodev\Drives\C\CLOUD\PCBLOX1.EGA")
        .Concat(File.ReadAllBytes(@"R:\retrodev\Drives\C\CLOUD\PCBLOX2.EGA"))
        .ToArray();
    bytesFromFile.Length.ToString("X").Dump();
    return bytesFromFile;
}

private void AddBloxToPixels(int bloxX, int bloxY)
{
    var bloxIndex = bloxY * BLOX_PER_ROW + bloxX;
    var topLeftX = bloxX * (BLOX_WIDTH + BORDER_SIZE);
    var topLeftY = bloxY * (BLOX_HEIGHT + BORDER_SIZE);
    _blox.AddBloxToPixels(bloxIndex, topLeftX, topLeftY, _pixelDisplay.Pixels);
}

private void ShowColorsUsage()
{
    for (var bloxIndex = 0; bloxIndex < TOTAL_BLOX; bloxIndex++)
    {
        for (var bloxY = 0; bloxY < BLOX_WIDTH; bloxY++)
        {
            for (var bloxX = 0; bloxX < BLOX_HEIGHT; bloxX++)
            {
                var pixelX = (int)(bloxIndex % BLOX_PER_ROW) * BLOX_WIDTH + bloxX;
                var pixelY = (int)(bloxIndex / BLOX_PER_ROW) * BLOX_HEIGHT + bloxY;
                if (_pixelDisplay.Pixels[pixelX, pixelY] == 6)
                {
                    Console.WriteLine($"Blox: {bloxIndex} ({bloxX}, {bloxY}) [{pixelX}, {pixelY}] = {_pixelDisplay.Pixels[pixelX, pixelY]}");
                }
            }
        }
    }
}
