<Query Kind="Program" />

private const int BYTES_IN_ROW = 30;
private const int PIXELS_IN_BYTE = 8;
private const int BYTE_BORDER_WIDTH = 0;
private const int ROWS = 57;
private const int PLANES = 4;

private readonly byte[,] _pixels = new byte[BYTES_IN_ROW * (PIXELS_IN_BYTE + BYTE_BORDER_WIDTH), ROWS];

void Main()
{
    var bytes = File.ReadAllBytes(@"R:\retrodev\Drives\C\CLOUD\PCSCORE2.EGA");
	bytes.Length.ToString("X").Dump();
    
	for(int plane = 0; plane < PLANES; plane++)
	{
		for (int row = 0; row < ROWS; row++)
		{
			for (int pixelByte = 0; pixelByte < BYTES_IN_ROW; pixelByte++)
			{
				byte mask = 0b10000000;
				byte data = bytes[plane * BYTES_IN_ROW * ROWS + row * BYTES_IN_ROW + pixelByte];
                if (plane == 2 && row >= 53 && row <= 55 && pixelByte >= 3 && pixelByte <= 27)
                {
                    //data = 255; // Turn on (make bright green) Hero Health Bar
                }
				for (int pixel = 0; pixel < 8; pixel++)
				{
					_pixels[pixelByte * (PIXELS_IN_BYTE + BYTE_BORDER_WIDTH) + pixel, row]
						+= ((data & mask) > 0 ? (byte)Math.Pow(2, 3 - plane) : (byte)0);
					mask >>= 1;
                }
                for (int pixel = 8; pixel < 8 + BYTE_BORDER_WIDTH; pixel++)
                {
                    _pixels[pixelByte * (PIXELS_IN_BYTE + BYTE_BORDER_WIDTH) + pixel, row] = 15;
                }
            }
        }
    }
    dumpPixels();
}

private void dumpPixels()
{
	for (int y = 0; y < ROWS; y++)
	{
        //if (y == 40) 
        //{
        //    Console.WriteLine();
        //}
        Console.Write("  ");
        var run = "";
        byte lastColor = 0;
        var maxX = BYTES_IN_ROW * (PIXELS_IN_BYTE + BYTE_BORDER_WIDTH) - 1;
		for (int x = 0; x <= maxX; x++)
        {
            byte pixel = _pixels[x, y];
            if (pixel == lastColor)
            {
                run += "██";
            }
            if (pixel != lastColor || x == maxX)
            {
                RenderRunOfPixels(run, lastColor);
                lastColor = pixel;
                run = "██";
            }
        }
        Console.WriteLine();
    }
}

public void RenderRunOfPixels(string run, byte color)
{
    var red = _palette[color, 0];
    var green = _palette[color, 1];
    var blue = _palette[color, 2];
    Console.Write(Util.WithStyle(run, $"color: RGB({red},{green},{blue})"));
}

byte[,] _palette = new byte[16, 3]
{
    /* 00 */ {0,0,0},
    /* 01 */ {0,0,170},
    /* 02 */ {0,170,0},
    /* 03 */ {0,170,170},
    /* 04 */ {170,0,0},
    /* 05 */ {170,0,170},
    /* 06 */ {170,85,0},
    /* 07 */ {170,170,170},
    /* 08 */ {85,85,85},
    /* 09 */ {85,85,255},
    /* 10 */ {85,255,85},
    /* 11 */ {85,255,255},
    /* 12 */ {255,85,85},
    /* 13 */ {255,85,255},
    /* 14 */ {255,255,85},
    /* 15 */ {255,255,255}
};