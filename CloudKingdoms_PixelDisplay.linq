<Query Kind="Program">
  <Namespace>System.Drawing</Namespace>
  <Namespace>System.Drawing.Imaging</Namespace>
</Query>

public class PixelDisplay
{
    private const int BITMAP_SCALE = 8;
    
    public int Width { get; }
    public int Height { get; }
    public byte[,] Pixels { get; }

    public PixelDisplay(int width, int height)
    {
        Width = width;
        Height = height;
        Pixels = new byte[width, height];
        InitializePalette();
    }
    
    public void RenderBitmap()
    {
        using (var bitmap = new System.Drawing.Bitmap(Width * BITMAP_SCALE, Height * BITMAP_SCALE, PixelFormat.Format24bppRgb))
        using (var graphics = Graphics.FromImage(bitmap))
        {
            for (int y = 0; y < Height; y++)
            {
                var nextLineStartX = 0;
                var lineLength = 0;
                byte lastColor = 0;
                for (int x = 0; x < Width; x++)
                {
                    byte pixel = Pixels[x, y];
                    if (pixel == lastColor)
                    {
                        lineLength++;
                    }
                    if (pixel != lastColor || x == Width - 1)
                    {
                        RenderRunOfPixels(graphics, nextLineStartX, y, lineLength, lastColor);
                        nextLineStartX += lineLength;
                        lastColor = pixel;
                        lineLength = 1;
                    }
                }
            }
            bitmap.Dump();
        }
    }

    public void RenderConsole()
    {
        for (int y = 0; y < Height; y++)
        {
            var run = "";
            byte lastColor = 0;
            for (int x = 0; x < Width; x++)
            {
                byte pixel = Pixels[x, y];
                Pixels[x, y] = 0;
                if (pixel == lastColor)
                {
                    run += "██";
                }
                if (pixel != lastColor || x == Width - 1)
                {
                    RenderRunOfPixels(run, lastColor);
                    lastColor = pixel;
                    run = "██";
                }
            }
            Console.WriteLine();
        }
    }

    private byte[,] _palette;
    
    private void InitializePalette()
    {
        _palette = new byte[16, 3]
        {
            /* 00 */ {0,0,0},
            /* 01 */ {0,0,170},
            /* 02 */ {0,170,0},
            /* 03 */ {0,170,170},
            /* 04 */ {170,0,0},
            /* 05 */ {170,0,170},
            /* 06 */ {170,85,0},
            /* 07 */ {170,170,170},
            /* 08 */ {85,85,85},
            /* 09 */ {85,85,255},
            /* 10 */ {85,255,85},
            /* 11 */ {85,255,255},
            /* 12 */ {255,85,85},
            /* 13 */ {255,85,255},
            /* 14 */ {255,255,85},
            /* 15 */ {255,255,255}
        };
    }

    private void RenderRunOfPixels(Graphics graphics, int x, int y, int lineLength, byte color)
    {
        var red = _palette[color, 0];
        var green = _palette[color, 1];
        var blue = _palette[color, 2];
        graphics.DrawLine(new Pen(Color.FromArgb(red, green, blue), BITMAP_SCALE), new Point(x * BITMAP_SCALE, y * BITMAP_SCALE), new Point((x + lineLength) * BITMAP_SCALE, y * BITMAP_SCALE));
    }

    public void RenderRunOfPixels(string run, byte color)
    {
        var red = _palette[color, 0];
        var green = _palette[color, 1];
        var blue = _palette[color, 2];
        Console.Write(Util.WithStyle(run, $"color: RGB({red},{green},{blue})"));
    }

}