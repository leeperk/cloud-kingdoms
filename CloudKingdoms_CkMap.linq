<Query Kind="Program" />

void Main()
{
    var bytes = File.ReadAllBytes(@"R:\retrodev\Drives\C\CLOUD\CKMAP.DTA");
    ("0x" + bytes.Length.ToString("X")).Dump();
    var mapIndex = 0;
    for(int offset = 0; offset < bytes.Length; offset += 0x200)
    {
        Console.WriteLine($"Map {mapIndex} - {mapNames[mapIndex]}");
        for (int s = 0; s < 0x200; s += 32)
        {
            for (int b = 0; b < 32; b++)
            {
                displayByte(bytes[offset + s + b]);
            }
            Console.WriteLine();
        }
        Console.WriteLine();
        mapIndex++;
    }
}

private void displayByte(byte value)
{
    Console.Write($"{value,2} ");
}

private string[] mapNames = {
    "CLOUD KINGDOM",
    "ICE KINGDOM",
    "GHOST KINGDOM",
    "ISLAND KINGDOM",
    "JUMPING KINGDOM",
    "UNSEEN KINGDOM",
    "CRYSTAL KINGDOM",
    "ARROW KINGDOM",
    "QUARTET KINGDOM",
    "BOX KINGDOM",
    "FLYING KINGDOM",
    "CHASE KINGDOM",
    "FIRE KINGDOM",
    "CORRIDOR KINGDOM",
    "DARK KINGDOM",
    "ACID KINGDOM",
    "PYRAMID KINGDOM",
    "PUZZLE KINGDOM",
    "SKI KINGDOM",
    "STONE KINGDOM",
    "GREEN KINGDOM",
    "ENIGMA KINGDOM",
    "DIABOLIC KINGDOM",
    "BARRIER KINGDOM",
    "SPIKY KINGDOM",
    "HAZARD KINGDOM",
    "MAGNETIC KINGDOM",
    "LAVA KINGDOM",
    "BRIDGE KINGDOM",
    "FORCE KINGDOM",
    "HOPSCOTCH KINGDOM",
    "AERIAL KINGDOM"
};