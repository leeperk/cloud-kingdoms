<Query Kind="Program" />

void Main()
{
    var bytes =
        File.ReadAllBytes(@"R:\retrodev\Drives\C\CLOUD\PCHERO3.EGA")
        .ToArray();
    bytes.Length.ToString("X").Dump();
    const byte bloxPerRow = 10;
    byte[,] pixels = new byte[32 * bloxPerRow, 32];
    var bloxIndex = 0;
    var startBlox = 0;
    for(int offset = 0; offset < bytes.Length; offset += 0x200)
    {
        string[] rows = new string[32];
        for (int plane = 0; plane < 4; plane++)
        {
            for (int row = 0; row < 32; row++)
            {
                for (int pixelByte = 0; pixelByte < 4; pixelByte++)
                {
                    byte mask = 0b10000000;
                    byte data = bytes[offset + plane * 32 * 4 + row * 4 + pixelByte];
                    for (int pixel = 0; pixel < 8; pixel++)
                    {
                        pixels[bloxIndex * 32 + pixelByte * 8 + pixel, row]
                            += ((data & mask) > 0 ? (byte)Math.Pow(2, 3 - plane) : (byte)0);
                        mask >>= 1;
                    }
                }
            }
        }
        
        if (bloxIndex == bloxPerRow - 1)
        {
            dumpTiles(startBlox, bloxIndex, pixels);
            bloxIndex = 0;
            Console.WriteLine();
            startBlox += bloxPerRow;
        }
        else
        {
            bloxIndex++;
        }
    }
    if (bloxIndex > 0)
    {
        dumpTiles(startBlox, bloxIndex, pixels);
    }
    bloxIndex.Dump();
}

private void dumpTiles(int startBlox, int bloxIndex, byte[,] pixels)
{
    Console.WriteLine($"\nBlox #: {startBlox}");
    for (int y = 0; y < 32; y++)
    {
        Console.Write("  ");
        var run = "";
        byte lastColor = 0;
        var maxX = 32 * (bloxIndex + 1) - 1;
        for (int x = 0; x <= maxX; x++)
        {
            if (x % 32 == 0)
            {
                RenderRunOfPixels(run, lastColor);
                run = "█";
                lastColor = 15;
            }
            byte pixel = pixels[x, y];
            pixels[x, y] = 0;
            if (pixel == lastColor)
            {
                run += "██";
            }
            if (pixel != lastColor || x == maxX)
            {
                RenderRunOfPixels(run, lastColor);
                lastColor = pixel;
                run = "██";
            }
        }
        Console.WriteLine();
    }
}

public void RenderRunOfPixels(string run, byte color)
{
    var red = _palette[color, 0];
    var green = _palette[color, 1];
    var blue = _palette[color, 2];
    Console.Write(Util.WithStyle(run, $"color: RGB({red},{green},{blue})"));
}

byte[,] _palette = new byte[16, 3]
{
    /* 00 */ {0,0,0},
    /* 01 */ {0,0,170},
    /* 02 */ {0,170,0},
    /* 03 */ {0,170,170},
    /* 04 */ {170,0,0},
    /* 05 */ {170,0,170},
    /* 06 */ {170,85,0},
    /* 07 */ {170,170,170},
    /* 08 */ {85,85,85},
    /* 09 */ {85,85,255},
    /* 10 */ {85,255,85},
    /* 11 */ {85,255,255},
    /* 12 */ {255,85,85},
    /* 13 */ {255,85,255},
    /* 14 */ {255,255,85},
    /* 15 */ {255,255,255}
};
