<Query Kind="Program" />

void Main()
{
    // Tune 16
    PlayTone(2, (0x4268 + 0x2710) - 0x2710);
    PlayTone(2, (0x4268 + 0x2710) - 0x2AF8);
    PlayTone(2, (0x4268 + 0x2710) - 0x2EE0);
    PlayTone(2, (0x4268 + 0x2710) - 0x32C8);
    PlayTone(2, (0x4268 + 0x2710) - 0x36B0);
    PlayTone(2, (0x4268 + 0x2710) - 0x3A98);
    PlayTone(2, (0x4268 + 0x2710) - 0x3E80);
    PlayTone(2, (0x4268 + 0x2710) - 0x4268);
}

void PlayTone(int duration, int tone)
{
    int milliseconds = duration * 45;
    int frequency = (int)(tone / 10);

    if (tone < 37)
    {
        Thread.Sleep(milliseconds * 2);
    }
    else
    {
        Console.Beep(frequency, milliseconds);
    }
}