<Query Kind="Program" />

private const int BYTES_IN_ROW = 40;
private const int PIXELS_IN_BYTE = 8;
private const int BYTE_BORDER_WIDTH = 0;
private const int ROWS = 200;
private const int PLANES = 4;

private readonly byte[,] _pixels = new byte[BYTES_IN_ROW * (PIXELS_IN_BYTE + BYTE_BORDER_WIDTH), ROWS];

void Main()
{
    var bytes = File.ReadAllBytes(@"R:\retrodev\Drives\C\CLOUD\TITLE.EGA");
	bytes.Length.ToString("X").Dump();
    
	for(int plane = 0; plane < PLANES; plane++)
	{
		for (int row = 0; row < ROWS; row++)
		{
			for (int pixelByte = 0; pixelByte < BYTES_IN_ROW; pixelByte++)
			{
				byte mask = 0b10000000;
				byte data = bytes[plane * BYTES_IN_ROW * ROWS + row * BYTES_IN_ROW + pixelByte];
				for (int pixel = 0; pixel < 8; pixel++)
				{
					_pixels[pixelByte * (PIXELS_IN_BYTE + BYTE_BORDER_WIDTH) + pixel, row]
						+= ((data & mask) > 0 ? (byte)Math.Pow(2, 3 - plane) : (byte)0);
					mask >>= 1;
                }
                for (int pixel = 8; pixel < 8 + BYTE_BORDER_WIDTH; pixel++)
                {
                    _pixels[pixelByte * (PIXELS_IN_BYTE + BYTE_BORDER_WIDTH) + pixel, row] = 15;
                }
            }
        }
    }
    RenderHeroBlock();
    dumpPixels();
}

private void dumpPixels()
{
    for (int y = 0; y < ROWS; y++)
	{
        if (y == 0x33 || y == 0x33 + 0x50)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }
        Console.Write("  ");
        var run = "";
        byte lastColor = 0;
        var maxX = BYTES_IN_ROW * (PIXELS_IN_BYTE + BYTE_BORDER_WIDTH) - 1;
		for (int x = 0; x <= maxX; x++)
        {
            byte pixel = _pixels[x, y];
            if (pixel == lastColor)
            {
                run += "██";
            }
            if (pixel != lastColor || x == maxX)
            {
                RenderRunOfPixels(run, lastColor);
                lastColor = pixel;
                run = "██";
            }
        }
        Console.WriteLine();
    }
}

public void RenderRunOfPixels(string run, byte color)
{
    var red = _palette[color, 0];
    var green = _palette[color, 1];
    var blue = _palette[color, 2];
    Console.Write(Util.WithStyle(run, $"color: RGB({red},{green},{blue})"));
}

private void RenderHeroBlock()
{
    // Green block 32x32 where the Hero will be drawn on the Title screen.
    // 0x4C is the Hero Top Y on the screen, 0x38 is the top Y of the CK Title on the screen
    // and 0x33 is the Y in the Title Data of the CK Title.  (0x33 - 0x38) + 0x4C is the Hero
    // Top Y in the Title Data (since we're drawing on the Title data here, not the screen).
    int heroTopYOnScreen = 0x4C;
    int heroLeftXOnScreen = 0xF8;
    int titleTopYOnScreen = 0x38;
    int titleTopYInTitleData = 0x33;

    for (int y = heroTopYOnScreen; y < heroTopYOnScreen + 32; y++)
    {
        for (int x = heroLeftXOnScreen; x < heroLeftXOnScreen + 32; x++)
        {
            _pixels[x, (titleTopYInTitleData - titleTopYOnScreen) + y] = 10;
        }
    }

    // Green block 32x32 where the Hero will be drawn on the Hall of Fame screen.
    heroTopYOnScreen = 0x0C;
    heroLeftXOnScreen = 0xDE;
    titleTopYOnScreen = 0x00;
    titleTopYInTitleData = 0x8C;
    for (int y = heroTopYOnScreen; y < heroTopYOnScreen + 32; y++)
    {
        for (int x = heroLeftXOnScreen; x < heroLeftXOnScreen + 32; x++)
        {
            _pixels[x, (titleTopYInTitleData - titleTopYOnScreen) + y] = 10;
        }
    }
}

byte[,] _palette = new byte[16, 3]
{
    /* 00 */ {0,0,0},
    /* 01 */ {0,0,170},
    /* 02 */ {0,170,0},
    /* 03 */ {0,170,170},
    /* 04 */ {170,0,0},
    /* 05 */ {170,0,170},
    /* 06 */ {170,85,0},
    /* 07 */ {170,170,170},
    /* 08 */ {85,85,85},
    /* 09 */ {85,85,255},
    /* 10 */ {85,255,85},
    /* 11 */ {85,255,255},
    /* 12 */ {255,85,85},
    /* 13 */ {255,85,255},
    /* 14 */ {255,255,85},
    /* 15 */ {255,255,255}
};