﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace PlayFieldRender
{
    public class PlayFieldRenderer
    {
        private const int FIRST_PLAYFIELD_TO_RENDER = 0;
        private const int LAST_PLAYFIELD_TO_RENDER = 31;
        private const int TOTAL_PLAYFIELDS_TO_RENDER = (LAST_PLAYFIELD_TO_RENDER - FIRST_PLAYFIELD_TO_RENDER + 1);
        private const bool SAVE_EACH_LEVEL = false;

        private static Blox _blox;

        static void Main(string[] args)
        {
            InitializePalette();
            var mapData = new MapData();
            _blox = new Blox();
            int[,] pixels2BitRGB = new int[1024, 512 * TOTAL_PLAYFIELDS_TO_RENDER];
            int[,] pixels8BitRGB = new int[1024, 512 * TOTAL_PLAYFIELDS_TO_RENDER];

            for (var level = FIRST_PLAYFIELD_TO_RENDER; level <= LAST_PLAYFIELD_TO_RENDER; level++)
            {
                var map = mapData.Maps[level];
                for (int y = 0; y < 16; y++)
                {
                    for (var x = 0; x < 32; x++)
                    {
                        var bloxIndex = mapData.GetBloxIndexFromMapValue(map[x, y]);
                        _blox.AddBloxToPixels(
                            bloxIndex,
                            map[x, y],
                            level - FIRST_PLAYFIELD_TO_RENDER,
                            x * 32,
                            (level - FIRST_PLAYFIELD_TO_RENDER) * 16 * 32 + y * 32,
                            pixels2BitRGB
                        );
                    }
                }
                ConvertLevel2BitRGBTo8BitRGB(pixels2BitRGB, pixels8BitRGB, level);
            }

            var pixelDisplay = new PixelDisplay(pixels8BitRGB);
            pixelDisplay.RenderBitmap(PostProcess);
        }

        private static string[] _levelNames =
        {
            "cloud kingdom",
            "ice kingdom",
            "ghost kingdom",
            "island kingdom",
            "jumping kingdom",
            "unseen kingdom",
            "crystal kingdom",
            "arrow kingdom",
            "quartet kingdom",
            "box kingdom",
            "flying kingdom",
            "chase kingdom",
            "fire kingdom",
            "corridor kingdom",
            "dark kingdom",
            "acid kingdom",
            "pyramid kingdom",
            "puzzle kingdom",
            "ski kingdom",
            "stone kingdom",
            "green kingdom",
            "enigma kingdom",
            "diabolic kingdom",
            "barrier kingdom",
            "spiky kingdom",
            "hazard kingdom",
            "magnetic kingdom",
            "lava kingdom",
            "bridge kingdom",
            "force kingdom",
            "hopscotch kingdom",
            "aerial kingdom"
        };

        private static void PostProcess(PictureBox pictureBox, Graphics graphics)
        {
            var hero = new Hero(graphics);
            var baddies = new Baddies(graphics);
            int bitmapScale = PixelDisplay.BITMAP_SCALE;
            if (SAVE_EACH_LEVEL)
            {
                for (var level = FIRST_PLAYFIELD_TO_RENDER; level <= LAST_PLAYFIELD_TO_RENDER; level++)
                {
                    SavePng(
                        $@".\Data\Level_{level:D2}.png",
                        pictureBox.Image,
                        bitmapScale,
                        0,
                        (level - FIRST_PLAYFIELD_TO_RENDER) * 16 * 32 + 0 * 32,
                        1024,
                        512
                    );
                }
            }

            for (var level = FIRST_PLAYFIELD_TO_RENDER; level <= LAST_PLAYFIELD_TO_RENDER; level++)
            {
                DrawText(
                    graphics,
                    _levelNames[level],
                    new FontFamily("Consolas"), FontStyle.Bold, 26.0f,
                    new Pen(Brushes.Black, 6), Brushes.White,
                    10, (level - FIRST_PLAYFIELD_TO_RENDER) * 512 * bitmapScale + 10
                );
                hero.DrawHero(level, (level - FIRST_PLAYFIELD_TO_RENDER) * 512, bitmapScale);
                baddies.DrawBaddies(level, (level - FIRST_PLAYFIELD_TO_RENDER) * 512, bitmapScale);
            }
            SavePng(
                $@".\Data\AllLevels.png",
                pictureBox.Image,
                bitmapScale,
                0,
                0,
                1024,
                512 * (LAST_PLAYFIELD_TO_RENDER - FIRST_PLAYFIELD_TO_RENDER + 1)
            );
        }

        private static void DrawText(Graphics graphics, string text, FontFamily fontFamily, FontStyle fontStyle, float emSize, Pen outlinePen, Brush fillBrush, int x, int y)
        {
            var textPath = new GraphicsPath();
            textPath.AddString(text, fontFamily, (int)fontStyle, emSize, new Point(x, y), new StringFormat());
            graphics.DrawPath(outlinePen, textPath);
            graphics.FillPath(fillBrush, textPath);
        }

        private static void DrawBaddie(Graphics graphics, int mapX, int mapY, int number, int bitmapScale)
        {
            float x = (mapX * 32) * bitmapScale;
            float y = (mapY * 32) * bitmapScale;
            graphics.FillEllipse(
                new SolidBrush(Color.FromArgb(127, Color.Black)),
                x,
                y,
                24 * bitmapScale,
                24 * bitmapScale
            );
            graphics.DrawEllipse(
                new Pen(Color.FromArgb(127, Color.White)),
                x,
                y,
                24 * bitmapScale,
                24 * bitmapScale
            );
            graphics.DrawString(number.ToString(), new Font("Consolas", 12), new SolidBrush(Color.FromArgb(192, Color.White)), x + 8 * bitmapScale, y + 8 * bitmapScale);
        }

        private static void SavePng(string filename, Image sourceImage, int bitmapScale, int topLeftX, int topLeftY, int width, int height)
        {
            using (var bitmap = new Bitmap(width * bitmapScale, height * bitmapScale))
            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.DrawImage(
                    sourceImage,
                    new Rectangle(0, 0, width * bitmapScale, height * bitmapScale),
                    topLeftX * bitmapScale,
                    topLeftY * bitmapScale,
                    width * bitmapScale, height * bitmapScale,
                    GraphicsUnit.Pixel);
                bitmap.Save(filename, ImageFormat.Png);
            }
        }

        private static int[,] ConvertLevel2BitRGBTo8BitRGB(int[,] pixels2BitRGB, int[,] pixels8BitRGB, int level)
        {
            for (int y = 0; y < 512; y++)
            {
                for (int x = 0; x < 1024; x++)
                {
                    pixels8BitRGB[x, (level - FIRST_PLAYFIELD_TO_RENDER) * 512 + y] = ConvertColor2BitRGBToColor8BitRGB(pixels2BitRGB[x, (level - FIRST_PLAYFIELD_TO_RENDER) * 512 + y], level);
                }
            }
            return pixels8BitRGB;
        }

        private static int ConvertColor2BitRGBToColor8BitRGB(int color2BitRGB, int level)
        {
            var fakeFloor = (color2BitRGB & 0b10000) > 0 ? 1 : 0;
            var wallFloor = (color2BitRGB & 0b100000) > 0 ? 1 : 0;
            color2BitRGB &= 0b01111;
            var indexForColor8BitRGB = _levelPalettes6Bit[level][color2BitRGB];
            if (indexForColor8BitRGB >= 0x10)
            {
                indexForColor8BitRGB -= 8;
            }

            var red = _palette8BitRGB[indexForColor8BitRGB, 0];
            var green = _palette8BitRGB[indexForColor8BitRGB, 1];
            var blue = _palette8BitRGB[indexForColor8BitRGB, 2];

            return (wallFloor << 25) + (fakeFloor << 24) + (red << 16) + (green << 8) + blue;
        }

        private static int[,] _palette8BitRGB;
        private static int[][] _levelPalettes6Bit;

        private static void InitializePalette()
        {
            _palette8BitRGB = new int[16, 3]
            {
            /* 00 */ {0,0,0},       // 0x00 = 000000 = (00, 00, 00)
            /* 01 */ {0,0,170},     // 0x01 = 000001 = (00, 00, 01)
            /* 02 */ {0,170,0},     // 0x02 = 000010 = (00, 01, 00)
            /* 03 */ {0,170,170},   // 0x03 = 000011 = (00, 01, 01)
            /* 04 */ {170,0,0},     // 0x04 = 000100 = (01, 00, 00)
            /* 05 */ {170,0,170},   // 0x05 = 000101 = (01, 00, 01)
            /* 06 */ {170,85,0},    // 0x06 = 010100 = (01, 10, 00)
            /* 07 */ {170,170,170}, // 0x07 = 000111 = (01, 01, 01)
            /* 08 */ {85,85,85},    // 0x10 = 111000 = (10, 10, 10)
            /* 09 */ {85,85,255},   // 0x11 = 111001 = (10, 10, 11)
            /* 10 */ {85,255,85},   // 0x12 = 111010 = (10, 11, 10)
            /* 11 */ {85,255,255},  // 0x13 = 111011 = (10, 11, 11)
            /* 12 */ {255,85,85},   // 0x14 = 111100 = (11, 10, 10)
            /* 13 */ {255,85,255},  // 0x15 = 111101 = (11, 10, 11)
            /* 14 */ {255,255,85},  // 0x16 = 111110 = (11, 11, 10)
            /* 15 */ {255,255,255}  // 0x17 = 111111 = (11, 11, 11)
            };

            _levelPalettes6Bit = new int[32][]
            {
            /* 00 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 01 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x03,0x07,0x10,0x11,0x12,0x13,0x11,0x15,0x16,0x17},
            /* 02 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 03 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 04 */ new int[] {0x00,0x01,0x02,0x03,0x02,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x12,0x15,0x16,0x17},
            /* 05 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 06 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x03,0x07,0x10,0x11,0x12,0x13,0x11,0x15,0x16,0x17},
            /* 07 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 08 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x05,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 09 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 10 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x03,0x07,0x10,0x11,0x12,0x13,0x11,0x15,0x16,0x17},
            /* 11 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 12 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 13 */ new int[] {0x00,0x01,0x02,0x03,0x02,0x05,0x02,0x07,0x10,0x11,0x12,0x13,0x12,0x15,0x16,0x17},
            /* 14 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x05,0x07,0x10,0x11,0x12,0x13,0x15,0x15,0x16,0x17},
            /* 15 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 16 */ new int[] {0x00,0x01,0x02,0x03,0x10,0x05,0x10,0x07,0x10,0x11,0x12,0x13,0x07,0x15,0x16,0x17},
            /* 17 */ new int[] {0x00,0x01,0x02,0x03,0x02,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x12,0x15,0x16,0x17},
            /* 18 */ new int[] {0x00,0x01,0x02,0x03,0x10,0x05,0x10,0x07,0x10,0x11,0x12,0x13,0x07,0x15,0x16,0x17},
            /* 19 */ new int[] {0x00,0x01,0x02,0x03,0x10,0x05,0x10,0x07,0x10,0x11,0x12,0x13,0x07,0x15,0x16,0x17},
            /* 20 */ new int[] {0x00,0x01,0x02,0x03,0x02,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x12,0x15,0x16,0x17},
            /* 21 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x11,0x07,0x10,0x11,0x12,0x13,0x03,0x15,0x16,0x17},
            /* 22 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x05,0x07,0x10,0x11,0x12,0x13,0x15,0x15,0x16,0x17},
            /* 23 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 24 */ new int[] {0x00,0x01,0x02,0x03,0x10,0x05,0x10,0x07,0x10,0x11,0x12,0x13,0x07,0x15,0x16,0x17},
            /* 25 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 26 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 27 */ new int[] {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17},
            /* 28 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x05,0x07,0x10,0x11,0x12,0x13,0x15,0x15,0x16,0x17},
            /* 29 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x03,0x07,0x10,0x11,0x12,0x13,0x11,0x15,0x16,0x17},
            /* 30 */ new int[] {0x00,0x01,0x02,0x03,0x10,0x05,0x10,0x07,0x10,0x11,0x12,0x13,0x07,0x15,0x16,0x17},
            /* 31 */ new int[] {0x00,0x01,0x02,0x03,0x01,0x05,0x05,0x07,0x10,0x11,0x12,0x13,0x15,0x15,0x16,0x17}
            };
        }
    }
}
