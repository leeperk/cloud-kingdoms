﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace PlayFieldRender
{
    public class PixelDisplay
    {
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        private Form _form;
        private Bitmap _bitmap;
        private Graphics _graphics;
        private PictureBox _pictureBox;
        private BackgroundWorker _backgroundWorker;
        private Action<Graphics>[] _postRenderActions;
        private bool _wasCancelled;

        public const int BITMAP_SCALE = 2;

        public int Width { get; }
        public int Height { get; }
        public int[,] Pixels { get; }
        public bool WasCancelled { get => _wasCancelled; }

        public PixelDisplay(int[,] pixels)
        {
            Pixels = pixels;
            Width = pixels.GetUpperBound(0) + 1;
            Height = pixels.GetUpperBound(1) + 1;
            _wasCancelled = false;

            _postRenderActions = new Action<Graphics>[]
            {
                //RenderStarPositions
                //RenderDiagonals
            };
        }

        public void RenderBitmap(Action<PictureBox, Graphics> postRenderAction = null)
        {
            using (_bitmap = new Bitmap(Width * BITMAP_SCALE, Height * BITMAP_SCALE, PixelFormat.Format24bppRgb))
            using (_graphics = Graphics.FromImage(_bitmap))
            {
                _form = new Form
                {
                    AutoScroll = true,
                    WindowState = FormWindowState.Maximized
                };
                _form.FormClosed += OnClose;

                _pictureBox = new PictureBox
                {
                    Width = _bitmap.Width,
                    Height = _bitmap.Height,
                    Image = _bitmap
                };
                _form.Controls.Add(_pictureBox);
                _backgroundWorker = new BackgroundWorker();
                _backgroundWorker.WorkerReportsProgress = true;
                _backgroundWorker.WorkerSupportsCancellation = true;
                _backgroundWorker.DoWork += DrawPixels;
                _backgroundWorker.ProgressChanged += OnNewLineDrawn;
                _backgroundWorker.RunWorkerCompleted += OnAllLinesDrawn;
                _backgroundWorker.RunWorkerAsync(postRenderAction);

                Application.Run(_form);
            }
        }

        private void DrawPixels(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            var postRenderAction = (Action<PictureBox, Graphics>)e.Argument;

            for (int y = 0; y < Height; y++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    _wasCancelled = true;
                    return;
                }
                var nextLineStartX = 0;
                var lineLength = 0;
                var lastColor = 0;
                for (int x = 0; x < Width; x++)
                {
                    var pixel = Pixels[x, y];
                    if (pixel == lastColor)
                    {
                        lineLength++;
                    }
                    if (pixel != lastColor || x == Width - 1)
                    {
                        RenderRunOfPixels(_graphics, nextLineStartX, y, lineLength, lastColor);
                        nextLineStartX += lineLength;
                        lastColor = pixel;
                        lineLength = 1;
                    }
                }
                _pictureBox.Invalidate(new Rectangle(0, y * BITMAP_SCALE, Width * BITMAP_SCALE, BITMAP_SCALE));
                worker.ReportProgress(y / Height, y);
            }
            postRenderAction?.Invoke(_pictureBox, _graphics);
            _pictureBox.Invalidate();
        }

        private void OnNewLineDrawn(object sender, ProgressChangedEventArgs e)
        {
            var y = (int)e.UserState * BITMAP_SCALE;
            if (y % (512 * BITMAP_SCALE) == 0)
            {
                _form.AutoScrollPosition = new Point(0, y);
            }
        }

        private void OnAllLinesDrawn(object sender, RunWorkerCompletedEventArgs e)
        {
            _form.AutoScrollPosition = new Point(0, 0);
        }

        private void OnClose(object sender, FormClosedEventArgs e)
        {
            _backgroundWorker.CancelAsync();
        }

        private void RenderDiagonals(Graphics graphics)
        {
            var patterns = new[] { 0x75, 0x79, 0x27, 0xE2 };
            var color = 11;
            foreach (var bp in patterns)
            {
                var cx = bp;

                var di = 0;

                var screenRectangle = new Rectangle(0, 0, 320 * BITMAP_SCALE, 200 * BITMAP_SCALE);

                do
                {
                    var preserveDi = di;
                    do
                    {
                        var x = (di % 40) * 8;
                        var y = (di - (di % 40)) / 40;
                        RenderRunOfPixels(graphics, x, y, 8, color);
                        _pictureBox.Invalidate(screenRectangle);
                        di += bp;
                    } while (di < 0x1F40);
                    Thread.Sleep(10);
                    di = preserveDi + 1;
                    cx--;
                } while (cx > 0);
                color++;
            }
        }

        private static void RenderStarPositions(Graphics graphics)
        {
            var coordinates = new int[,] {
                        { 000, 045}, { 192, 109}, { 248, 029}, { 040, 105}, { 216, 097},
                        { 000, 093}, { 008, 070}, { 240, 040}, { 032, 052}, { 152, 050},
                        { 056, 011}, { 200, 091}, { 176, 052}, { 088, 124}, { 312, 140},
                        { 144, 115}, { 240, 097}, { 216, 027}, { 056, 029}, { 144, 113},
                        { 000, 043}, { 120, 048}, { 008, 104}, { 144, 083}, { 176, 033},
                        { 224, 041}, { 248, 081}, { 208, 027}, { 184, 069}, { 112, 007},
                        { 216, 103}, { 104, 087}, { 120, 006}, { 192, 089}, { 152, 027},
                        { 056, 121}, { 064, 131}, { 056, 001}, { 136, 138}, { 008, 127},
                        { 104, 015}, { 256, 031}, { 240, 009}, { 176, 106}, { 152, 086},
                        { 096, 032}, { 240, 067}, { 024, 059}, { 280, 058}, { 128, 007},
                        { 048, 026}, { 248, 128}, { 256, 077}, { 088, 099}, { 216, 031},
                        { 304, 027}, { 168, 138}, { 096, 020}, { 040, 079}, { 272, 042}
                    };
            var offsets = new int[]
            {
                        0x0708, 0x1120, 0x04A7, 0x106D, 0x0F43,
                        0x0E88, 0x0AF1, 0x065E, 0x0824, 0x07E3,
                        0x01BF, 0x0E51, 0x0836, 0x136B, 0x1607,
                        0x120A, 0x0F46, 0x0453, 0x048F, 0x11BA,
                        0x06B8, 0x078F, 0x1041, 0x0D0A, 0x053E,
                        0x0684, 0x0CC7, 0x0452, 0x0ADF, 0x0126,
                        0x1033, 0x0DA5, 0x00FF, 0x0E00, 0x044B,
                        0x12EF, 0x1480, 0x002F, 0x15A1, 0x13D9,
                        0x0265, 0x04F8, 0x0186, 0x10A6, 0x0D83,
                        0x050C, 0x0A96, 0x093B, 0x0933, 0x0128,
                        0x0416, 0x141F, 0x0C28, 0x0F83, 0x04F3,
                        0x045E, 0x15A5, 0x032C, 0x0C5D, 0x06B2,
            };

            for (int i = 0; i < 60; i++)
            {
                int x = coordinates[i, 0];
                int y = coordinates[i, 1];
                int offset = offsets[i];
                var offset_x = (offset % 40) * 8;
                var offset_y = (offset - (offset % 40)) / 40;

                graphics.FillRectangle(
                    Brushes.Aquamarine,
                    x * BITMAP_SCALE, y * BITMAP_SCALE,
                    10, 10
                );
                graphics.FillRectangle(
                    Brushes.DarkCyan,
                    offset_x * BITMAP_SCALE, offset_y * BITMAP_SCALE,
                    5, 5
                );
            }
        }

        private void RenderRunOfPixels(Graphics graphics, int x, int y, int lineLength, int color)
        {
            var alpha = (color & 0b_01_00000000_00000000_00000000) > 0 ? 127 : 255;
            var isWallFloor = (color & 0b_10_00000000_00000000_00000000) > 0;
            var red = (color & 0b_11111111_00000000_00000000) >> 16;
            var green = (color & 0b_00000000_11111111_00000000) >> 8;
            var blue = (color & 0b_00000000_00000000_11111111);
            graphics.DrawLine(
                new Pen(Color.FromArgb(alpha, red, green, blue), BITMAP_SCALE),
                new Point(x * BITMAP_SCALE + BITMAP_SCALE / 2, y * BITMAP_SCALE + BITMAP_SCALE / 2),
                new Point((x + lineLength) * BITMAP_SCALE + BITMAP_SCALE / 2, y * BITMAP_SCALE + BITMAP_SCALE / 2)
            );
            if (isWallFloor)
            {
                for(var hashX = x; hashX < x + lineLength; hashX++)
                {
                    if ((hashX % 2) == y % 2)
                    {
                        graphics.DrawLine(
                            new Pen(Color.Black, BITMAP_SCALE),
                            new Point(hashX * BITMAP_SCALE + BITMAP_SCALE / 2, y * BITMAP_SCALE + BITMAP_SCALE / 2),
                            new Point((hashX + 1) * BITMAP_SCALE + BITMAP_SCALE / 2, y * BITMAP_SCALE + BITMAP_SCALE / 2)
                        );
                    }
                }
            }
        }
    }
}
