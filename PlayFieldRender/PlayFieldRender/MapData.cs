﻿using System.IO;

namespace PlayFieldRender
{
    public class MapData
    {
        public const byte TOTAL_MAPS = 32;
        public const byte MAP_WIDTH = 32;
        public const byte MAP_HEIGHT = 16;

        private static readonly string FILENAME = @".\Data\CKMAP.DTA";
        private byte[] _mapToBloxLookup;

        public byte[][,] Maps { get; } = new byte[TOTAL_MAPS][,];

        public MapData()
        {
            LoadFile();
            LoadMapToBloxLookup();
        }

        public byte GetBloxIndexFromMapValue(byte mapValue)
        {
            return _mapToBloxLookup[mapValue];
        }

        private void LoadFile()
        {
            var bytes = File.ReadAllBytes(FILENAME);
            for (int mapIndex = 0; mapIndex < TOTAL_MAPS; mapIndex++)
            {
                Maps[mapIndex] = new byte[MAP_WIDTH, MAP_HEIGHT];
                for (int y = 0; y < MAP_HEIGHT; y++)
                {
                    for (int x = 0; x < MAP_WIDTH; x++)
                    {
                        Maps[mapIndex][x, y] = bytes[mapIndex * MAP_WIDTH * MAP_HEIGHT + y * MAP_WIDTH + x];
                    }
                }
            }
        }

        private void LoadMapToBloxLookup()
        {
            _mapToBloxLookup = new byte[] {
                0x25,  // 00 = No Floor, Fall Through, Stars shine through
                0x6B,  // 01 = Floor Current, no item
                0x28,  // 02 = Blue 4-spike wall
                0x2E,  // 03 = Floor Red
                0x2D,  // 04 = Floor Purple
                0x06,  // 05 = Brown Pipes Wall Horizontal, Floor Top, Connector Bottom
                0x14,  // 06 = Brown Pipes Wall Vertical, Floor Left, Connector Right
                0x1A,  // 07 = Brown Pipes Wall Top Left Corner, Floor Outer
                0x1C,  // 08 = Brown Pipes Wall Top Right Corner, Floor Outer
                0x1B,  // 09 = Brown Pipes Wall Bottom Left Corner, Floor Outer
                0x08,  // 10 = Brown Pipes Wall Bottom Right Corner, Floor Outer
                0x18,  // 11 = Brown Pipes Wall Down T
                0x15,  // 12 = Brown Pipes Wall Up T
                0x19,  // 13 = Brown Pipes Wall Right T
                0x17,  // 14 = Brown Pipes Wall Left T
                0x16,  // 15 = Brown Pipes Wall Plus
                0x04,  // 16 = Brown Pipes Wall Horizontal, Black Top, Connector Bottom
                0x26,  // 17 = Brown Pipes Wall Horizontal, Floor Top, Black Bottom
                0x01,  // 18 = Brown Pipes Wall Vertical, Black Left, Connector Right
                0x1E,  // 19 = Brown Pipes Wall Vertical, Floor Left, Black Right
                0x00,  // 20 = Brown Pipes Wall Top Left Corner, Black Outer
                0x09,  // 21 = Brown Pipes Wall Top Right Corner, Black Outer
                0x02,  // 22 = Brown Pipes Wall Bottom Left Corner, Black Outer
                0x1D,  // 23 = Brown Pipes Wall Bottom Right Corner, Black Outer
                0x20,  // 24 = Bumper Horizontal
                0x27,  // 25 = Bumper Vertical
                0x1F,  // 26 = Bumper Top Left Corner
                0x21,  // 27 = Bumper Top Right Corner
                0x2B,  // 28 = Bumper Bottom Left Corner
                0x2C,  // 29 = Bumper Bottom Right Corner
                0x37,  // 30 = Bumper Down T
                0x38,  // 31 = Bumper Up T
                0x36,  // 32 = Bumper Right T
                0x39,  // 33 = Bumper Left T
                0x2A,  // 34 = Bumper Dot
                0x22,  // 35 = Bumper Vertical Top End Cap
                0x29,  // 36 = Bumper Vertical Bottom End Cap
                0x32,  // 37 = Bumper Horizontal Left End Cap
                0x33,  // 38 = Bumper Horizontal Right End Cap
                0x0C,  // 39 = Push Left
                0x0D,  // 40 = Push Right
                0x0E,  // 41 = Push Up
                0x0B,  // 42 = Push Down
                0x10,  // 43 = Push Up Left
                0x0F,  // 44 = Push Up Right
                0x11,  // 45 = Push Down Left
                0x12,  // 46 = Push Down Right
                0x0A,  // 47 = Door Vertical Pipes
                0x07,  // 48 = Door Horizontal Pipes
                0x03,  // 49 = Floor Ice
                0x6B,  // 50 = Floor Current, No Item, Fall Through
                0x25,  // 51 = Floor Black?
                0x2F,  // 52 = Floor Green
                0x13,  // 53 = Bumper Crazy
                0x24,  // 54 = Magenet
                0x6B,  // 55 = Floor Current, No Item, Wall For Enemy, Not Wall For Player
                0x23,  // 56 = Floor Pink
                0x6B,  // 57 = Floor Current, No Item, Only Used In Unseen Kingdom (no idea why 1 wasn't used)
                0x25,  // 58 = Floor Black?
                0x6B,  // 59 = Floor Current, No Item, Wall Horizontal
                0x6B,  // 60 = Floor Current, No Item, Wall Vertical
                0x6B,  // 61 = Floor Current, No Item, Wall Top Left Corner
                0x6B,  // 62 = Floor Current, No Item, Wall Top Right Corner
                0x6B,  // 63 = Floor Current, No Item, Wall Bottom Left Corner
                0x6B,  // 64 = Floor Current, No Item, Wall Bottom Right Corner
                0x6B,  // 65 = Floor Current, No Item, Wall Down T
                0x6B,  // 66 = Floor Current, No Item, Wall Up T
                0x6B,  // 67 = Floor Current, No Item, Wall Right T
                0x6B,  // 68 = Floor Current, No Item, Wall Left T
                0x6B,  // 69 = Floor Current, No Item, Wall Plus
                0x69,  // 70 = Floor Current, Gem Item
                0x6A,  // 71 = Floor Current, Paint Item
                0x67,  // 72 = Floor Current, Wing Item
                0x64,  // 73 = Floor Current, Key Item
                0x65,  // 74 = Floor Current, Clock Item
                0x66,  // 75 = Floor Current, Soda Item
                0x68,  // 76 = Floor Current, Hammer Item
                0x41,  // 77 = Brown Stone Wall
                0x48,  // 78 = Green Skull
                0x4B,  // 79 = Yellow Pillar
                0x4C,  // 80 = Floor (?) Brown 4 Spheres
                0x4E,  // 81 = Brown Ball, Black Outer
                0x42,  // 82 = Door Vertical Stone
                0x43,  // 83 = Door Horizontal Stone
                0x4A,  // 84 = Door Vertical Green
                0x49,  // 85 = Door Horizontal Green
                0x6E,  // 86 = Cloud 3
                0x6F,  // 87 = Cloud 4
                0x70,  // 88 = Cloud 5
                0x71,  // 89 = Cloud 6
                0x72,  // 90 = Cloud 7
                0x73,  // 91 = Cloud 8
                0x74,  // 92 = Cloud 9
                0x75,  // 93 = Cloud 10
                0x6C,  // 94 = Cloud 1
                0x6D,  // 95 = Cloud 2
                0x76,  // 96 = Cloud 11
                0x77   // 97 = Cloud 12
            };
        }
    }
}
