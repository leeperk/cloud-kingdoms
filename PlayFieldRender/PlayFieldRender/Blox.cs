﻿using System;
using System.IO;
using System.Linq;

namespace PlayFieldRender
{
    public class Blox
    {
        const int BLOX_WIDTH = 32;
        const int BLOX_HEIGHT = 32;
        const int PIXELS_PER_BYTE = 8;
        const int BLOX_WIDTH_IN_BYTES = BLOX_WIDTH / PIXELS_PER_BYTE;
        const int PLANES = 4;
        const bool DRAW_BLOX_GRID = false;

        private byte[] _bytesFromFile;

        private int[] _levelFloorBloxIndexes;

        public Blox()
        {
            LoadFile();
            LoadLevelFloorBloxIndexes();
        }

        public void AddBloxToPixels(int bloxIndex, int mapData, int level, int topLeftX, int topLeftY, int[,] pixels)
        {
            for (int plane = 0; plane < PLANES; plane++)
            {
                for (int row = 0; row < BLOX_HEIGHT; row++)
                {
                    if (DRAW_BLOX_GRID && row % 32 == 31)
                    {
                        continue;
                    }
                    for (int pixelByte = 0; pixelByte < BLOX_WIDTH_IN_BYTES; pixelByte++)
                    {
                        int mask = 0b10000000;
                        int data = (bloxIndex >= 100 && bloxIndex <= 107)
                            ? GetFloorByteForYAndRowByteIndex(_levelFloorBloxIndexes[level], bloxIndex - 20, plane, row, pixelByte)
                            : GetByteForYAndRowByteIndex(bloxIndex, plane, row, pixelByte);
                        for (int pixel = 0; pixel < 8; pixel++)
                        {
                            var pixelX = topLeftX + pixelByte * PIXELS_PER_BYTE + pixel;
                            var pixelY = topLeftY + row;
                            var value = ((data & mask) > 0 ? (int)Math.Pow(2, 3 - plane) : 0);
                            if (DRAW_BLOX_GRID && pixelByte == BLOX_WIDTH_IN_BYTES - 1 && pixel == 7)
                            {
                                value = 0;
                            }
                            pixels[pixelX, pixelY] += value;
                            if (mapData == 50)
                            {
                                pixels[pixelX, pixelY] |= 0b10000;
                            } else if (mapData >=59 && mapData <= 69)
                            {
                                pixels[pixelX, pixelY] |= 0b100000;
                            }
                            
                            mask >>= 1;
                        }
                    }
                }
            }
        }

        private byte GetByteForYAndRowByteIndex(int bloxIndex, int plane, int row, int pixelByte)
        {
            var offsetInFile = GetOffsetInFile(bloxIndex, plane, row, pixelByte);

            return _bytesFromFile[offsetInFile];
        }

        private byte GetFloorByteForYAndRowByteIndex(int floorBloxIndex, int itemBloxIndex, int plane, int row, int pixelByte)
        {
            var floorByte = _bytesFromFile[GetOffsetInFile(floorBloxIndex, plane, row, pixelByte)];
            var itemByte = _bytesFromFile[GetOffsetInFile(itemBloxIndex, plane, row, pixelByte)];
            var maskByte = _bytesFromFile[GetOffsetInFile(itemBloxIndex + 10, plane, row, pixelByte)];

            floorByte &= (byte)(~maskByte);
            itemByte &= maskByte;

            return (byte)(floorByte | itemByte);
        }

        private int GetOffsetInFile(int bloxIndex, int plane, int row, int pixelByte)
            => bloxIndex * (BLOX_WIDTH_IN_BYTES * BLOX_HEIGHT * PLANES)
               + plane * BLOX_WIDTH_IN_BYTES * BLOX_HEIGHT
               + row * BLOX_WIDTH_IN_BYTES
               + pixelByte;

        private void LoadFile()
        {
            _bytesFromFile =
                File.ReadAllBytes(@".\Data\PCBLOX1.EGA")
                .Concat(File.ReadAllBytes(@".\Data\PCBLOX2.EGA"))
                .ToArray();
        }

        private void LoadLevelFloorBloxIndexes()
        {
            _levelFloorBloxIndexes = new int[] {
                0x05, 0x28, 0x05, 0x4D,
                0x4F, 0x3F, 0x3C, 0x40,
                0x3D, 0x40, 0x47, 0x4D,
                0x3F, 0x4B, 0x4C, 0x05,
                0x3D, 0x45, 0x3F, 0x40,
                0x4C, 0x4B, 0x4F, 0x05,
                0x40, 0x45, 0x3C, 0x3F,
                0x05, 0x28, 0x45, 0x3D
            };
        }
    }
}
