﻿using System.Drawing;

namespace PlayFieldRender
{
    public class Hero
    {
        private const int X = 0;
        private const int Y = 1;
        private readonly Graphics _graphics;

        public Hero(Graphics graphics)
        {
            _graphics = graphics;
        }

        private readonly int[,] _heroStartingMapCoordinates = new int[,]
        {
            {0x02, 0x02},
            {0x0F, 0x01},
            {0x0F, 0x08},
            {0x02, 0x02},
            {0x0B, 0x07},
            {0x18, 0x07},
            {0x01, 0x0E},
            {0x02, 0x02},
            {0x0F, 0x07},
            {0x10, 0x04},
            {0x10, 0x0B},
            {0x1E, 0x09},
            {0x02, 0x07},
            {0x0A, 0x08},
            {0x0C, 0x0D},
            {0x10, 0x07},
            {0x14, 0x06},
            {0x04, 0x01},
            {0x01, 0x03},
            {0x02, 0x0E},
            {0x10, 0x07},
            {0x02, 0x01},
            {0x1E, 0x0D},
            {0x15, 0x07},
            {0x19, 0x0B},
            {0x13, 0x08},
            {0x14, 0x08},
            {0x0B, 0x0D},
            {0x10, 0x09},
            {0x02, 0x0E},
            {0x10, 0x07},
            {0x10, 0x05}
        };

        internal void DrawHero(int level, int levelStartingY, int bitmapScale)
        {
            var mapX = _heroStartingMapCoordinates[level, X];
            var mapY = _heroStartingMapCoordinates[level, Y];

            float x = (mapX * 32 + 6) * bitmapScale;
            float y = levelStartingY * bitmapScale + (mapY * 32 + 4) * bitmapScale;
            _graphics.FillEllipse(
                new SolidBrush(Color.FromArgb(127, Color.Green)),
                x,
                y,
                22 * bitmapScale,
                22 * bitmapScale
            );
            _graphics.DrawEllipse(
                new Pen(Color.FromArgb(127, Color.Black), bitmapScale),
                x,
                y,
                22 * bitmapScale,
                22 * bitmapScale
            );
        }
    }
}
